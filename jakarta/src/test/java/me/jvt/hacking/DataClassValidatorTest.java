package me.jvt.hacking;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

class DataClassValidatorTest {
  @Disabled(
      "unit tests would be expected here, but are left to be implemented, as the core functionality this repo is sharing is how to write Unit Integration tests")
  @Test
  void todo() {
    fail("TODO");
  }
}
