package me.jvt.hacking;

@ValidData
public class DataClass {
  private String nonNullValue;

  @ValidData
  public DataClass() {}

  @ValidData
  public DataClass(String nonNullValue) {
    this.nonNullValue = nonNullValue;
  }

  public String getNonNullValue() {
    return nonNullValue;
  }

  public void setNonNullValue(String nonNullValue) {
    this.nonNullValue = nonNullValue;
  }
}
