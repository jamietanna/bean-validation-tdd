package me.jvt.hacking;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({
  ElementType.TYPE_USE,
  ElementType.TYPE,
  ElementType.METHOD,
  ElementType.PARAMETER,
  ElementType.FIELD,
  ElementType.CONSTRUCTOR
})
@Constraint(validatedBy = DataClassValidator.class)
public @interface ValidData {

  String message() default "The DataClass was not validated correctly";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
