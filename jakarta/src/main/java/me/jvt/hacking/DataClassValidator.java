package me.jvt.hacking;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class DataClassValidator implements ConstraintValidator<ValidData, DataClass> {
  @Override
  public boolean isValid(DataClass value, ConstraintValidatorContext context) {
    return value.getNonNullValue() != null;
  }
}
