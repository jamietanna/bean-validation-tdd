package me.jvt.hacking;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.annotation.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import javax.validation.*;
import javax.validation.constraints.NotNull;
import javax.validation.executable.ExecutableValidator;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class ValidDataTest {
  private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
  private final ExecutableValidator executableValidator = validator.forExecutables();

  /** This is the annotation on the top-level of {@link DataClass}. */
  @Nested
  class TYPE_Validation {

    @Nested
    class WhenInvalid {
      @Test
      void hasViolation() {
        DataClass dataClass = new DataClass();

        Set<ConstraintViolation<Object>> violations = validate(validator, dataClass);

        assertThat(violations).hasSize(1);
      }

      @Test
      void hasMessage() {
        DataClass dataClass = new DataClass();

        Set<ConstraintViolation<Object>> violations = validate(validator, dataClass);

        assertThat(first(violations).getMessage())
            .isEqualTo("The DataClass was not validated correctly");
      }
    }

    @Nested
    class WhenValid {
      @Test
      void doesNotHaveViolations() {
        DataClass dataClass = new DataClass();
        dataClass.setNonNullValue("non-null");

        Set<ConstraintViolation<Object>> violations = validate(validator, dataClass);

        assertThat(violations).isEmpty();
      }
    }

    private Set<ConstraintViolation<Object>> validate(Validator validator, DataClass dataClass) {
      Set<ConstraintViolation<Object>> violations = validator.validate(dataClass);
      return violations;
    }
  }

  /**
   * This is the annotation used to validate a given field in a class, as seen in {@link
   * ClassWithField}.
   */
  @Nested
  class FIELD_Validation {

    @Nested
    class WhenInvalid {
      @Test
      void hasViolation() {
        ClassWithField classWithField = new ClassWithField();
        classWithField.dataClass = new DataClass();

        Set<ConstraintViolation<Object>> violations = validate(classWithField);

        assertThat(violations).hasSize(1);
      }

      @Test
      void hasMessage() {
        ClassWithField classWithField = new ClassWithField();
        classWithField.dataClass = new DataClass();

        Set<ConstraintViolation<Object>> violations = validate(classWithField);

        assertThat(first(violations).getMessage())
            .isEqualTo("The DataClass was not validated correctly");
      }
    }

    @Nested
    class WhenValid {
      @Test
      void doesNotHaveViolations() {
        ClassWithField classWithField = new ClassWithField();
        classWithField.dataClass = new DataClass();
        classWithField.dataClass.setNonNullValue("non-null");

        Set<ConstraintViolation<Object>> violations = validate(classWithField);

        assertThat(violations).isEmpty();
      }
    }

    private Set<ConstraintViolation<Object>> validate(ClassWithField classWithField) {
      return validator.validate(classWithField);
    }

    class ClassWithField {
      @ValidData private DataClass dataClass;
    }
  }

  /** This is the annotation used to validate the return type for a given method. */
  @Nested
  class METHOD_Validation {
    @Nested
    class WhenInvalid {
      @Test
      void hasViolation() {
        DataClass dataClass = new DataClass();

        Set<ConstraintViolation<Object>> violations = validate(dataClass);

        assertThat(violations).hasSize(1);
      }

      @Test
      void hasMessage() {
        DataClass dataClass = new DataClass();

        Set<ConstraintViolation<Object>> violations = validate(dataClass);

        assertThat(first(violations).getMessage())
            .isEqualTo("The DataClass was not validated correctly");
      }
    }

    @Nested
    class WhenValid {
      @Test
      void doesNotHaveViolations() {
        DataClass dataClass = new DataClass();
        dataClass.setNonNullValue("non-null");

        Set<ConstraintViolation<Object>> violations = validate(dataClass);

        assertThat(violations).isEmpty();
      }
    }

    private Set<ConstraintViolation<Object>> validate(DataClass dataClass) {
      Method method;
      try {
        method = getClass().getMethod("methodToCheck");
      } catch (NoSuchMethodException e) {
        throw new IllegalStateException("Could not find Method", e);
      }
      return executableValidator.validateReturnValue(this, method, dataClass);
    }

    @ValidData
    public DataClass methodToCheck() {
      return null; // in this example, it only needs to have the right type signature
    }
  }

  /**
   * This is the annotation used in an argument to a method, for instance the <code>methodToCheck
   * </code> method below.
   */
  @Nested
  class PARAMETER_Validation {
    @Nested
    class WhenInvalid {
      @Test
      void hasViolation() {
        DataClass dataClass = new DataClass();

        Set<ConstraintViolation<Object>> violations = validate(dataClass);

        assertThat(violations).hasSize(1);
      }

      @Test
      void hasMessage() {
        DataClass dataClass = new DataClass();

        Set<ConstraintViolation<Object>> violations = validate(dataClass);

        assertThat(first(violations).getMessage())
            .isEqualTo("The DataClass was not validated correctly");
      }
    }

    @Nested
    class WhenValid {
      @Test
      void doesNotHaveViolations() {
        DataClass dataClass = new DataClass();
        dataClass.setNonNullValue("non-null");

        Set<ConstraintViolation<Object>> violations = validate(dataClass);

        assertThat(violations).isEmpty();
      }
    }

    private Set<ConstraintViolation<Object>> validate(DataClass dataClass) {
      Method method;
      try {
        method = getClass().getMethod("methodToCheck", DataClass.class);
      } catch (NoSuchMethodException e) {
        throw new IllegalStateException("Could not find Method", e);
      }
      Object[] params = {dataClass};
      return executableValidator.validateParameters(this, method, params);
    }

    /**
     * Only required to have the right type signature.
     *
     * @param clazz the class that needs to be validated by the annotation
     */
    public void methodToCheck(@ValidData DataClass clazz) {}
  }

  /**
   * This is for the annotation where we want to validate the object returned from a constructor is
   * valid.
   */
  @Nested
  class CONSTRUCTOR_Validation {
    @Nested
    class ZeroArgsConstructor {
      /**
       * Can only ever be invalid, as there's no way to set it as valid on the zero-args
       * constructor.
       */
      @Nested
      class WhenInvalid {
        @Test
        void hasViolation() {
          Set<ConstraintViolation<Object>> violations = validate();

          assertThat(violations).hasSize(1);
        }

        @Test
        void hasMessage() {
          Set<ConstraintViolation<Object>> violations = validate();

          assertThat(first(violations).getMessage())
              .isEqualTo("The DataClass was not validated correctly");
        }
      }
    }

    @Nested
    class OneArgConstructor {
      @Nested
      class WhenInvalid {
        @Test
        void hasViolation() {
          Set<ConstraintViolation<Object>> violations = validate();

          assertThat(violations).hasSize(1);
        }

        @Test
        void hasMessage() {
          Set<ConstraintViolation<Object>> violations = validate();

          assertThat(first(violations).getMessage())
              .isEqualTo("The DataClass was not validated correctly");
        }
      }

      @Nested
      class WhenValid {
        @Test
        void doesNotHaveViolations() {
          DataClass dataClass = new DataClass("not-null");

          Set<ConstraintViolation<Object>> violations = validate(dataClass);

          assertThat(violations).isEmpty();
        }
      }
    }

    private Set<ConstraintViolation<Object>> validate() {
      return validate(new DataClass());
    }

    private Set<ConstraintViolation<Object>> validate(DataClass created) {
      Constructor<DataClass> constructor;
      try {
        constructor = DataClass.class.getConstructor(String.class);
      } catch (NoSuchMethodException e) {
        throw new IllegalStateException("Could not find Constructor", e);
      }

      return executableValidator.validateConstructorReturnValue(constructor, created);
    }
  }

  @Disabled("Not yet implemented")
  @Nested
  class LOCAL_VARIABLE_Validation {
    // is not implemented
  }

  /** This is for annotations that can be composed with other annotations. */
  @Nested
  class ANNOTATION_TYPE_Validation {
    @Nested
    class WhenInvalid {
      @Test
      void hasViolation() {
        ClassWithMetaAnnotation clazz = new ClassWithMetaAnnotation();

        Set<ConstraintViolation<Object>> violations = validate(clazz);

        assertThat(violations).hasSize(1);
      }

      @Test
      void hasMessage() {
        ClassWithMetaAnnotation clazz = new ClassWithMetaAnnotation();

        Set<ConstraintViolation<Object>> violations = validate(clazz);

        assertThat(first(violations).getMessage())
            .isEqualTo("The DataClass was not validated correctly");
      }
    }

    @Nested
    class WhenValid {
      @Test
      void doesNotHaveViolations() {
        ClassWithMetaAnnotation clazz = new ClassWithMetaAnnotation();
        clazz.dataClass = new DataClass();
        clazz.dataClass.setNonNullValue("non-null");

        Set<ConstraintViolation<Object>> violations = validate(clazz);

        assertThat(violations).isEmpty();
      }
    }

    private Set<ConstraintViolation<Object>> validate(ClassWithMetaAnnotation clazz) {
      return validator.validate(clazz);
    }

    private class ClassWithMetaAnnotation {
      @MetaAnnotation DataClass dataClass = new DataClass();
    }
  }

  @Disabled("Not yet implemented")
  @Nested
  class PACKAGE_Validation {
    // is not implemented
  }

  @Disabled("Not yet implemented")
  @Nested
  class TYPE_PARAMETER_Validation {
    // is not implemented
  }

  /**
   * This is the annotation on i.e. containerised types, i.e. the parameter added to a <code>List
   * </code>, as seen in {@link ClassWithContainer}. It can also be used for other purposes, like
   * when casting.
   */
  @Nested
  class TYPE_USE_Validation {

    @Nested
    class WhenInvalid {
      @Test
      void hasViolation() {
        ClassWithContainer clazz = new ClassWithContainer();
        DataClass dataClass = new DataClass();
        clazz.dataClasses = Collections.singletonList(dataClass);

        Set<ConstraintViolation<Object>> violations = validate(clazz);

        assertThat(violations).hasSize(1);
      }

      @Test
      void hasMessage() {
        ClassWithContainer clazz = new ClassWithContainer();
        DataClass dataClass = new DataClass();
        clazz.dataClasses = Collections.singletonList(dataClass);

        Set<ConstraintViolation<Object>> violations = validate(clazz);

        assertThat(first(violations).getMessage())
            .isEqualTo("The DataClass was not validated correctly");
      }
    }

    @Nested
    class WhenValid {
      @Test
      void doesNotHaveViolations() {
        ClassWithContainer clazz = new ClassWithContainer();
        DataClass dataClass = new DataClass();
        dataClass.setNonNullValue("No nulls here");
        clazz.dataClasses = Collections.singletonList(dataClass);

        Set<ConstraintViolation<Object>> violations = validate(clazz);

        assertThat(violations).isEmpty();
      }
    }

    private Set<ConstraintViolation<Object>> validate(ClassWithContainer clazz) {
      return validator.validate(clazz);
    }

    private class ClassWithContainer {
      private List<@ValidData DataClass> dataClasses;
    }
  }

  @Disabled("Not yet implemented")
  @Nested
  class MODULE_Validation {
    // is not implemented
  }

  /**
   * This is the annotation used to validate that a {@link Record}'s constructor parameters are
   * valid.
   */
  @Nested
  class RECORD_COMPONENT_Validation {

    @Nested
    class WhenInvalid {
      @Test
      void hasViolation() {
        Set<ConstraintViolation<Object>> violations = validate(null);

        assertThat(violations).hasSize(1);
      }

      @Test
      void hasMessage() {
        Set<ConstraintViolation<Object>> violations = validate(null);

        assertThat(first(violations).getMessage()).isEqualTo("The ID should be non-null");
      }
    }

    @Nested
    class WhenValid {
      @Test
      void doesNotHaveViolations() {
        Set<ConstraintViolation<Object>> violations = validate("foo");

        assertThat(violations).isEmpty();
      }
    }

    @NotNull
    @Target({ElementType.RECORD_COMPONENT, ElementType.PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    @Constraint(validatedBy = {})
    @ReportAsSingleViolation
    @Documented
    @interface ValidId {
      String message() default "The ID should be non-null";

      Class<?>[] groups() default {};

      Class<? extends Payload>[] payload() default {};
    }

    public record RecordDataClass(@ValidId String id) {}

    private Set<ConstraintViolation<Object>> validate(String id) {
      Constructor<RecordDataClass> constructor;
      try {
        // or `.getDeclaredConstructor` if `private`/package-private `record` class
        constructor = RecordDataClass.class.getConstructor(String.class);
      } catch (NoSuchMethodException e) {
        throw new IllegalStateException("Could not find Constructor", e);
      }

      Object[] args = {id};
      return executableValidator.validateConstructorParameters(constructor, args);
    }
  }

  @ValidData
  @Target({ElementType.ANNOTATION_TYPE, ElementType.PARAMETER, ElementType.FIELD})
  @Retention(RetentionPolicy.RUNTIME)
  @Constraint(validatedBy = {})
  @Documented
  private @interface MetaAnnotation {
    String message() default "This isn't used, as the @ValidData gets triggered instead";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
  }

  /**
   * Helper method for working with {@link Set>}s, as there's no i.e. <code>.get(0)</code>.
   *
   * @param set the set
   * @return the first element
   */
  private ConstraintViolation<Object> first(Set<ConstraintViolation<Object>> set) {
    return set.iterator().next();
  }
}
