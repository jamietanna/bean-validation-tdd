package me.jvt.hacking;

import java.lang.annotation.*;
import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({
  ElementType.ANNOTATION_TYPE,
  ElementType.TYPE_USE,
  ElementType.TYPE,
  ElementType.METHOD,
  ElementType.PARAMETER,
  ElementType.FIELD,
  ElementType.CONSTRUCTOR
})
@Constraint(validatedBy = DataClassValidator.class)
public @interface ValidData {

  String message() default "The DataClass was not validated correctly";

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};
}
