package me.jvt.hacking;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DataClassValidator implements ConstraintValidator<ValidData, DataClass> {
  @Override
  public boolean isValid(DataClass value, ConstraintValidatorContext context) {
    return value.getNonNullValue() != null;
  }
}
